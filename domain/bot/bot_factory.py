from data.data_destinations.aggregator_data_destination import AggregatorDataDestination
from data.repositories.aggergator_articles_destination_repository import AggregatorArticlesDestinationRepository
from domain.bot.bot import Bot


def create_bot(config, articles_source_repository, articles_collection_period, routing_key, logger):
    return Bot(
        articles_source_repository=articles_source_repository,
        articles_destination_repository=AggregatorArticlesDestinationRepository(
            AggregatorDataDestination(
                aggregator_server_url=config.server,
                port=config.port,
                username=config.username,
                password=config.password,
                routing_key=routing_key,
            ),
        ),
        articles_collection_period_sec=articles_collection_period,
        logger=logger,
    )
