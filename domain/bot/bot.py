import time


class Bot:
    def __init__(self, articles_source_repository, articles_destination_repository, articles_collection_period_sec,
                 logger):
        self.articles_source_repository = articles_source_repository
        self.articles_destination_repository = articles_destination_repository
        self.articles_collection_period_sec = articles_collection_period_sec
        self.logger = logger

    def run(self):
        while True:
            try:
                self.logger.log('Downloading articles...')
                articles = self.articles_source_repository.get_articles()
                try:
                    self.logger.log('Sending articles...')
                    self.articles_destination_repository.add_articles(articles)
                except Exception as e:
                    self.logger.log('Error during sending articles: ' + str(e))
            except Exception as e:
                self.logger.log('Error during downloading articles: ' + str(e))
            finally:
                try:
                    self.logger.log('Sleeping for ' + str(self.articles_collection_period_sec) + ' seconds...')
                    time.sleep(self.articles_collection_period_sec)
                except KeyboardInterrupt:
                    self.logger.log('Bot closed')
                    exit()
