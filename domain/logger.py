import datetime


class Logger:
    def __init__(self, bot_name):
        self.bot_name = bot_name

    def log(self, message):
        print('[' + self.bot_name + ' - ' + str(datetime.datetime.now()) + ']: ' + message)
