class Article:
    def __init__(self, source, title, description, url, image_url, publication_date, content, authors, tags, category):
        self.source = source
        """String. Where was the article published"""
        self.title = title
        """String. Title of the article"""
        self.description = description
        """String. Description of the article"""
        self.url = url
        """String. Url to the article"""
        self.image_url = image_url
        """String. Url to the primary article image"""
        self.publication_date = publication_date
        """Int. Unix Epoch Time. Date of publication"""
        self.content = content
        """String. The article text"""
        self.authors = authors
        """List<String>. Authors of the article"""
        self.tags = tags
        """List<String>Tags of the article"""
        self.category = category
        """String. Category of the article"""
