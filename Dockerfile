FROM python:3.10
WORKDIR /app
COPY . .

RUN python -m pip install -r requirements.txt
RUN chmod +x ./scripts/run_bots.sh
ENTRYPOINT ["./scripts/run_bots.sh"]
