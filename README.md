# News bots
This repository contains bots which download news from different  
sources and send these news to the aggregator server via a  
RabbigMQ queue.
## Installation
1. Make sure you have Python 3.9.6 or newer installed
2. run `pip install -r requirements.txt`
## Running
Create the following variables:  
- NEWS_API_KEY - contains api key to NewsApi
- NEWSDATA_KEY - contains api key to Newsdata
- $MEDIASTACK_KEY - contains api key to Mediastack
- RABBITMQ_ADDRESS
- RABBITMQ_PORT
- RABBITMQ_USERNAME
- RABBITMQ_PASSWORD
Run `./scripts/run_bots.sh <RabbitMQ server address>`  
if the bots run on the same machine as RabbitMQ just pass localhost
as RabbitMQ server address