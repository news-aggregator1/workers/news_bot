import getopt
import os
import sys

from config import Config


def read_config(require_api_key=True):
    try:
        aggregator_server_url = os.getenv('RABBITMQ_ADDRESS')
        aggregator_server_port = os.getenv('RABBITMQ_PORT')
        rabbitmq_username = os.getenv('RABBITMQ_USERNAME')
        rabbitmq_password = os.getenv('RABBITMQ_PASSWORD')
        opts, _ = getopt.getopt(sys.argv[1:], 'a:', ['api_key ='])
        api_key = None
        if len(opts) >= 1:
            api_key = opts[0][1]
        elif require_api_key:
            raise Exception('--api_key not specified')
        return Config(
            server=aggregator_server_url,
            port=aggregator_server_port,
            username=rabbitmq_username,
            password=rabbitmq_password,
            articles_source_api_key=api_key,
        )
    except Exception as e:
        print('WRONG USAGE. USAGE: --server <server with queue IP> [--api_key <API key>]\nError: ' + str(e))
        return None
