from data.models.mappers.newsdata_article_mapper import convert_newsdata_article_to_article
from domain.repositories.articles_source_repository import ArticlesSourceRepository


class NewsdataRepository(ArticlesSourceRepository):
    def __init__(self, newsdata_data_source):
        self.newsdata_data_source = newsdata_data_source

    def get_articles(self):
        newsdata_articles = self.newsdata_data_source.get_articles()
        articles = []
        for newsdata_article in newsdata_articles:
            articles.append(convert_newsdata_article_to_article(newsdata_article))

        return articles
