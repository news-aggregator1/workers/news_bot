from data.data_sources.reuters_data_source import ReutersDataSource
from data.models.mappers.reuters_article_mapper import convert_reuters_article_to_article
from domain.repositories.articles_source_repository import ArticlesSourceRepository


class ReutersRepository(ArticlesSourceRepository):
    def __init__(self, reuters_data_source):
        self.reuters_data_source = reuters_data_source

    def get_articles(self):
        reuters_articles = self.reuters_data_source.get_articles()
        return [convert_reuters_article_to_article(article, ReutersDataSource.URL) for article in reuters_articles]
