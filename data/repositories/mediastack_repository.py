from data.models.mappers.mediastack_article_mapper import convert_mediastack_article_to_article
from domain.repositories.articles_source_repository import ArticlesSourceRepository


class MediastackRepository(ArticlesSourceRepository):
    def __init__(self, mediastack_data_source):
        self.mediastack_data_source = mediastack_data_source

    def get_articles(self):
        mediastack_articles = self.mediastack_data_source.get_articles()
        articles = []
        for mediastack_article in mediastack_articles:
            articles.append(convert_mediastack_article_to_article(mediastack_article))

        return articles
