from data.models.mappers.data_article_mapper import convert_article_to_data_article
from domain.repositories.articles_destination_repository import ArticlesDestinationRepository


class AggregatorArticlesDestinationRepository(ArticlesDestinationRepository):
    def __init__(self, aggregator_data_destination):
        self.aggregator_data_destination = aggregator_data_destination

    def add_articles(self, articles):
        data_articles = [convert_article_to_data_article(article) for article in articles]
        self.aggregator_data_destination.add_articles(data_articles)
