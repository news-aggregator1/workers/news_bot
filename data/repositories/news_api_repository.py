from data.models.mappers.news_api_article_mapper import convert_news_api_article_to_article
from domain.repositories.articles_source_repository import ArticlesSourceRepository


class NewsApiRepository(ArticlesSourceRepository):
    def __init__(self, news_api_data_source):
        self.news_api_data_source = news_api_data_source

    def get_articles(self):
        news_api_articles = self.news_api_data_source.get_articles()
        articles = []
        for news_api_article in news_api_articles:
            articles.append(convert_news_api_article_to_article(news_api_article))

        return articles
