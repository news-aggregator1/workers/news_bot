class NewsdataArticle:
    def __init__(self, title, link, source_id, keywords, creator, image_url, description, pub_date, content, categories):
        self.title = title
        self.link = link
        self.source_id = source_id
        self.keywords = keywords
        self.creator = creator
        self.image_url = image_url
        self.description = description
        self.pub_date = pub_date
        self.content = content
        self.categories = categories

    @staticmethod
    def from_json(json):
        return NewsdataArticle(
            title=json['title'] if json['title'] is not None else '',
            link=json['link'] if json['link'] is not None else '',
            source_id=json['source_id'] if json['source_id'] is not None else '',
            keywords=json['keywords'] if json['keywords'] is not None else [],
            creator=json['creator'] if json['creator'] is not None else [],
            image_url=json['image_url'] if json['image_url'] is not None else '',
            description=json['description'] if json['description'] is not None else '',
            pub_date=json['pubDate'] if json['pubDate'] is not None else '',
            content=json['content'] if json['content'] is not None else '',
            categories=json['category'] if json['category'] is not None else [],
        )
