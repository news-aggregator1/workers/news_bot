class MediastackArticle:
    def __init__(self, author, title, description, url, source, image, category, published_at):
        self.author = author
        self.title = title
        self.description = description
        self.url = url
        self.source = source
        self.image = image
        self.category = category
        self.published_at = published_at

    @staticmethod
    def from_json(json):
        return MediastackArticle(
            author=json['author'] if json['author'] is not None else '',
            title=json['title'] if json['title'] is not None else '',
            description=json['description'] if json['description'] is not None else '',
            url=json['url'] if json['url'] is not None else '',
            source=json['source'] if json['source'] is not None else '',
            image=json['image'] if json['image'] is not None else '',
            category=json['category'] if json['category'] is not None else '',
            published_at=json['published_at'] if json['published_at'] is not None else '',
        )
