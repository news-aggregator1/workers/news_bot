class NewsApiArticle:
    def __init__(self, source, author, title, description, url, url_to_image, published_at, content):
        self.source = source
        self.author = author
        self.title = title
        self.description = description
        self.url = url
        self.url_to_image = url_to_image
        self.published_at = published_at
        self.content = content

    @staticmethod
    def from_json(json):
        return NewsApiArticle(
            source=json['source']['name'] if json['source']['name'] is not None else '',
            author=json['author'] if json['author'] is not None else '',
            title=json['title'] if json['title'] is not None else '',
            description=json['description'] if json['description'] is not None else '',
            url=json['url'] if json['url'] is not None else '',
            url_to_image=json['urlToImage'] if json['urlToImage'] is not None else '',
            published_at=json['publishedAt'] if json['publishedAt'] is not None else '',
            content=json['content'] if json['content'] is not None else '',
        )
