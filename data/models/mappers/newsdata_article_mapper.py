from data.models.data_article import DataArticle
from data.models.mappers.data_article_mapper import convert_data_article_to_article
from datetime import datetime


def convert_newsdata_article_to_article(newsdata_article):
    epoch_date = None
    try:
        epoch_date = int(datetime.strptime(newsdata_article.pub_date, '%Y-%m-%d %H:%M:%S').timestamp())
    except ValueError:
        pass

    return convert_data_article_to_article(
        DataArticle(
            source=newsdata_article.source_id,
            title=newsdata_article.title,
            authors=newsdata_article.creator,
            description=newsdata_article.description,
            url=newsdata_article.link,
            image_url=newsdata_article.image_url,
            publication_date=epoch_date,
            content=newsdata_article.content,
            tags=newsdata_article.keywords,
            category=newsdata_article.categories[0] if len(newsdata_article.categories) > 0 else '',
        )
    )
