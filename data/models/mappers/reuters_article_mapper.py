from datetime import datetime

from data.models.data_article import DataArticle
from data.models.mappers.data_article_mapper import convert_data_article_to_article


def convert_reuters_article_to_article(reuters_article, base_url):
    epoch_date = None
    try:
        epoch_date = int(datetime.strptime(reuters_article.published_time[:-5], '%Y-%m-%dT%H:%M:%S').timestamp())
    except ValueError:
        pass

    source = ''
    if reuters_article.source is not None and 'original_name' in reuters_article.source:
        source = reuters_article.source['original_name']
    authors = []
    if reuters_article.authors is not None:
        authors_filtered = filter(lambda author: 'name' in author, reuters_article.authors)
        authors = [author['name'] for author in authors_filtered]
    image_url = ''
    if reuters_article.thumbnail is not None and 'url' in reuters_article.thumbnail:
        image_url = reuters_article.thumbnail['url']

    return convert_data_article_to_article(
        DataArticle(
            source=source,
            title=reuters_article.title,
            authors=authors,
            description=reuters_article.description,
            url=base_url + reuters_article.relative_url,
            image_url=image_url,
            publication_date=epoch_date,
            content='',
            tags=reuters_article.tags,
            category=reuters_article.category,
        )
    )
