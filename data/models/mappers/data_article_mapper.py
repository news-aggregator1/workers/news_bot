from data.models.data_article import DataArticle
from domain.models.article import Article

MAX_SOURCE_LENGTH = 100
MAX_TITLE_LENGTH = 500
MAX_DESCRIPTION_LENGTH = 500
MAX_URL_LENGTH = 500
MAX_IMAGE_URL_LENGTH = 500
MAX_CONTENT_LENGTH = 10000
MAX_NUMBER_OF_AUTHORS = 10
MAX_AUTHOR_NAME_LENGTH = 100
MAX_NUMBER_OF_TAGS = 20
MAX_TAG_LENGTH = 50
MAX_CATEGORY_LENGTH = 100
UNKNOWN_PUBLICATION_DATE = -1


def convert_data_article_to_article(data_article):
    return Article(
        source=data_article.source[:MAX_SOURCE_LENGTH],
        title=data_article.title[:MAX_TITLE_LENGTH],
        description=data_article.description[:MAX_DESCRIPTION_LENGTH],
        url=data_article.url[:MAX_URL_LENGTH],
        image_url=data_article.image_url[:MAX_IMAGE_URL_LENGTH],
        publication_date=data_article.publication_date if data_article.publication_date is not None
        else UNKNOWN_PUBLICATION_DATE,
        content=data_article.content[:MAX_CONTENT_LENGTH],
        authors=[author[:MAX_AUTHOR_NAME_LENGTH]
                 for author in data_article.authors[:min(MAX_NUMBER_OF_AUTHORS, len(data_article.authors))]],
        tags=[tag[:MAX_TAG_LENGTH] for tag in data_article.tags[:min(MAX_NUMBER_OF_TAGS, len(data_article.tags))]],
        category=data_article.category[:MAX_CATEGORY_LENGTH],
    )


def convert_article_to_data_article(article):
    return DataArticle(
        source=article.source,
        title=article.title,
        description=article.description,
        url=article.url,
        image_url=article.image_url,
        publication_date=article.publication_date,
        content=article.content,
        authors=article.authors,
        tags=article.tags,
        category=article.category,
    )
