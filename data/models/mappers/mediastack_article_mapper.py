from data.models.data_article import DataArticle
from data.models.mappers.data_article_mapper import convert_data_article_to_article
from datetime import datetime


def convert_mediastack_article_to_article(mediastack_article):
    epoch_date = None
    try:
        epoch_date = int(datetime.strptime(mediastack_article.published_at[:-6], '%Y-%m-%dT%H:%M:%S').timestamp())
    except ValueError:
        pass

    return convert_data_article_to_article(
        DataArticle(
            source=mediastack_article.source,
            title=mediastack_article.title,
            authors=[mediastack_article.author] if mediastack_article.author != '' else [],
            description=mediastack_article.description,
            url=mediastack_article.url,
            image_url=mediastack_article.image,
            publication_date=epoch_date,
            content='',
            tags=[],
            category=mediastack_article.category,
        )
    )
