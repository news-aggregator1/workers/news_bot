from data.models.data_article import DataArticle
from data.models.mappers.data_article_mapper import convert_data_article_to_article
from datetime import datetime


def convert_news_api_article_to_article(news_api_article):
    epoch_date = None
    try:
        epoch_date = int(datetime.strptime(news_api_article.published_at, '%Y-%m-%dT%H:%M:%SZ').timestamp())
    except ValueError:
        pass

    return convert_data_article_to_article(
        DataArticle(
            source=news_api_article.source,
            title=news_api_article.title,
            authors=[news_api_article.author] if news_api_article.author != '' else [],
            description=news_api_article.description,
            url=news_api_article.url,
            image_url=news_api_article.url_to_image,
            publication_date=epoch_date,
            content=news_api_article.content,
            tags=[],
            category='',
        )
    )
