class ReutersArticle:
    def __init__(self, title, description, published_time, source, authors, thumbnail, tags, category, relative_url):
        self.title = title
        self.description = description
        self.published_time = published_time
        self.source = source
        self.authors = authors
        self.thumbnail = thumbnail
        self.tags = tags
        self.category = category
        self.relative_url = relative_url

    @staticmethod
    def from_json(json):
        tags = []
        if 'kicker' in json and 'name' in json['kicker']:
            tags.append(json['kicker']['name'])
        category = ''
        if 'primary_section' in json and 'name' in json['primary_section']:
            category = json['primary_section']['name']

        return ReutersArticle(
            title=json['title'],
            description=json['description'] if 'description' in json else '',
            published_time=json['published_time'] if 'published_time' in json else None,
            source=json['source'] if 'source' in json else None,
            authors=json['authors'] if 'authors' in json else [],
            thumbnail=json['thumbnail'] if 'thumbnail' in json else None,
            tags=tags,
            category=category,
            relative_url=json['canonical_url']
        )
