from data.data_destinations.data_destination import DataDestination
import json
import pika


def _convert_articles_to_json(data_articles):
    json_articles = [article.to_json() for article in data_articles]

    return json.dumps(json_articles)


class AggregatorDataDestination(DataDestination):
    EXCHANGE_NAME = 'articles_exchange'
    EXCHANGE_TYPE = 'direct'

    def __init__(self, aggregator_server_url, port, username, password, routing_key):
        self.aggregator_server_url = aggregator_server_url
        self.port = port
        self.username = username
        self.password = password
        self.routing_key = routing_key

    def add_articles(self, data_articles):
        json_data = _convert_articles_to_json(data_articles)
        credentials = pika.PlainCredentials(self.username, self.password)
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=self.aggregator_server_url,
                port=self.port,
                credentials=credentials,
            ),
        )
        channel = connection.channel()
        channel.exchange_declare(exchange=self.EXCHANGE_NAME, exchange_type=self.EXCHANGE_TYPE)
        channel.basic_publish(exchange=self.EXCHANGE_NAME, routing_key=self.routing_key, body=json_data.encode())
        connection.close()
