import requests as requests

from data.data_sources.data_source import DataSource
from data.models.mediastack_article import MediastackArticle


class MediastackDataSource(DataSource):
    URL = 'http://api.mediastack.com/v1/news?languages=en&limit=100'

    def __init__(self, api_key):
        self.api_key = api_key

    def get_articles(self):
        response = requests.get(self.URL + '&access_key=' + self.api_key).json()
        if 'error' not in response:
            articles = []
            for article_json in response['data']:
                articles.append(MediastackArticle.from_json(article_json))
            return articles
        else:
            raise Exception('Error during downloading news from mediastack: ' + response['error']['message'])
