import requests
import json

from data.data_sources.data_source import DataSource
from data.models.reuters_article import ReutersArticle


def _extract_categories(site_content):
    categories_start_phrase = r'{"site-hierarchy-by-name-v1":{"{\"hierarchy_name\":\"Website\",\"website\":\"reuters\"}":{"data":{"statusCode":200,"message":"Success","result":{"id":"/","website":"reuters","name":"Reuters","node_type":"section","children":'
    categories_list_idx = site_content.index(categories_start_phrase) + len(categories_start_phrase)
    categories_str = site_content[categories_list_idx:]
    square_braces_count = 1
    for i in range(1, len(categories_str)):
        if square_braces_count == 0:
            return _extract_categories_paths(json.loads(categories_str[:i]))
        if categories_str[i] == ']':
            square_braces_count -= 1
        elif categories_str[i] == '[':
            square_braces_count += 1
    return []


def _extract_categories_paths(categories_json):
    """categories_json is a list of json categories with subcategories, e.g. world/africa, business/aerospace-defense.
    The returned paths will direct to the main categories and to the subcategories"""
    paths = []
    for category_json in categories_json:
        if 'id' in category_json:
            paths.append(category_json['id'])
        if 'children' in category_json:
            for subcategory in category_json['children']:
                if 'id' in subcategory:
                    paths.append(subcategory['id'])

    return filter(is_valid_category, paths)


def is_valid_category(category_path):
    if '/markets/global-market-data/' in category_path or \
            '/investigates/' in category_path or \
            '/graphics/' in category_path or \
            '/pictures/' in category_path or \
            '/video/' in category_path:
        return False
    else:
        return True


class ReutersDataSource(DataSource):
    URL = "https://www.reuters.com"

    def __init__(self, logger):
        self.logger = logger

    def get_articles(self):
        response_home = requests.get(self.URL)
        if response_home.ok:
            home_content = response_home.content.decode(response_home.apparent_encoding)
            categories = _extract_categories(home_content)
            articles_json = self._extract_articles(home_content, self.URL)
            for category in categories:
                response_category = requests.get(self.URL + category)
                if response_category.ok:
                    category_content = response_category.content.decode(response_category.apparent_encoding)
                    articles_json.extend(self._extract_articles(category_content, self.URL + category))
                else:
                    raise Exception(
                        'Error during downloading Reuters ' + category + '. Code: ' + str(
                            response_category.status_code))
            return [ReutersArticle.from_json(article_json) for article_json in articles_json]
        else:
            raise Exception('Error during downloading Reuters home page. Code: ' + str(response_home.status_code))

    def _extract_articles(self, site_content, site_name):
        self.logger.log('Extracting articles from ' + site_name)
        try:
            articles_start_phrase = '"fetch_type":"collection","articles":'
            articles_list_idx = site_content.index(articles_start_phrase) + len(articles_start_phrase)
            articles_str = site_content[articles_list_idx:]
            square_braces_count = 1
            for i in range(1, len(articles_str)):
                if square_braces_count == 0:
                    return json.loads(articles_str[:i])
                if articles_str[i] == ']':
                    square_braces_count -= 1
                elif articles_str[i] == '[':
                    square_braces_count += 1
            return []
        except Exception as e:
            self.logger.log('Error during extracting articles from ' + site_name + ". Error: " + str(e))
            return []
