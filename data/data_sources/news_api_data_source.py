import requests as requests

from data.data_sources.data_source import DataSource
from data.models.news_api_article import NewsApiArticle


class NewsApiDataSource(DataSource):
    URL = 'https://newsapi.org/v2/top-headlines?country=us'

    def __init__(self, api_key):
        self.api_key = api_key

    def get_articles(self):
        response = requests.get(self.URL + '&apiKey=' + self.api_key).json()
        if response['status'] == 'ok':
            articles = []
            for article_json in response['articles']:
                articles.append(NewsApiArticle.from_json(article_json))
            return articles
        else:
            raise Exception('Error during downloading news from NewsApi. Code: ' + response['code'] +
                            'message: ' + response['message'])
