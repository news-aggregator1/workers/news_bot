import requests

from data.data_sources.data_source import DataSource
from data.models.newsdata_article import NewsdataArticle


class NewsdataDataSource(DataSource):
    URL = 'https://newsdata.io/api/1/news?language=en'

    def __init__(self, api_key):
        self.api_key = api_key

    def get_articles(self):
        response = requests.get(self.URL + '&apikey=' + self.api_key).json()
        if response['status'] == 'success':
            articles = []
            for article_json in response['results']:
                articles.append(NewsdataArticle.from_json(article_json))

            return articles
        else:
            raise Exception('Error during downloading news from Newsdata. Code: ' + response['code'] +
                            'message: ' + response['message'])
