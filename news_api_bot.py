from domain.bot.bot_factory import create_bot
from config_reader import read_config
from data.data_sources.news_api_data_source import NewsApiDataSource
from data.repositories.news_api_repository import NewsApiRepository
from domain.logger import Logger


DAILY_REQUESTS_LIMIT = 100

config = read_config()
if config is not None:
    news_api_bot = create_bot(
        config=config,
        articles_source_repository=NewsApiRepository(NewsApiDataSource(api_key=config.articles_source_api_key)),
        articles_collection_period=0.9 * ((24 * 3600) / DAILY_REQUESTS_LIMIT),
        routing_key='article-queue01',
        logger=Logger('NEWS_API_BOT'),
    )
    news_api_bot.run()
else:
    print('Could not create NewsApi bot')
