#!/bin/sh
python news_api_bot.py --api_key $NEWS_API_KEY &
python newsdata_bot.py --api_key $NEWSDATA_KEY &
python mediastack_bot.py --api_key $MEDIASTACK_KEY &
python reuters_bot.py &
