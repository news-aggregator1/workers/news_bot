from domain.bot.bot_factory import create_bot
from config_reader import read_config
from data.data_sources.newsdata_data_source import NewsdataDataSource
from data.repositories.newsdata_repository import NewsdataRepository
from domain.logger import Logger


DAILY_CREDIT = 200
NEWS_PER_CREDIT = 10
NEWS_PER_REQUEST = 10
NEWS_PER_DAY = DAILY_CREDIT * NEWS_PER_CREDIT

config = read_config()
if config is not None:
    newsdata_bot = create_bot(
        config=config,
        articles_source_repository=NewsdataRepository(NewsdataDataSource(api_key=config.articles_source_api_key)),
        articles_collection_period=0.9 * ((24 * 3600) / (NEWS_PER_DAY / NEWS_PER_REQUEST)),
        routing_key='article-queue02',
        logger=Logger('NEWSDATA_BOT'),
    )
    newsdata_bot.run()
else:
    print('Could not create Newsdata bot')
