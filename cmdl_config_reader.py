import getopt
import sys

from config import Config


def read_config(require_api_key=True):
    try:
        opts, _ = getopt.getopt(sys.argv[1:], 's:a:', ['server =', 'api_key ='])
        server = opts[0][1]
        api_key = None
        if len(opts) >= 2:
            api_key = opts[1][1]
        elif require_api_key:
            raise Exception('--api_key not specified')
        return Config(server=server, articles_source_api_key=api_key)
    except Exception as e:
        print('WRONG USAGE. USAGE: --server <server with queue IP> [--api_key <API key>]\nError: ' + str(e))
        return None
