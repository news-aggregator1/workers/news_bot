import pika

from data.data_destinations.aggregator_data_destination import AggregatorDataDestination

# First run rabbitMQ: /usr/local/opt/rabbitmq/sbin/rabbitmq-server


def queue_callback(ch, method, properties, body):
    print('Received: %r' % body)


connection = pika.BlockingConnection(pika.ConnectionParameters(
    host='localhost',
    port=5672
))
channel = connection.channel()
channel.exchange_declare(exchange='articles_exchange', exchange_type='direct')
result = channel.queue_declare(queue='', exclusive=True)
queue_name = result.method.queue
channel.queue_bind(exchange='articles_exchange', queue=queue_name, routing_key='article-queue04')
channel.queue_bind(exchange='articles_exchange', queue=queue_name, routing_key='article-queue03')
channel.queue_bind(exchange='articles_exchange', queue=queue_name, routing_key='article-queue02')
channel.queue_bind(exchange='articles_exchange', queue=queue_name, routing_key='article-queue01')
channel.basic_consume(queue=queue_name, auto_ack=True, on_message_callback=queue_callback)
channel.start_consuming()
