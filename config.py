class Config:
    def __init__(self, server, port, username, password, articles_source_api_key):
        self.server = server
        self.articles_source_api_key = articles_source_api_key
        self.port = port
        self.username = username
        self.password = password
