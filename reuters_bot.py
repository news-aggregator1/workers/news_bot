from domain.bot.bot_factory import create_bot
from config_reader import read_config
from data.data_sources.reuters_data_source import ReutersDataSource
from data.repositories.reuters_repository import ReutersRepository
from domain.logger import Logger


config = read_config(require_api_key=False)
if config is not None:
    logger = Logger('REUTERS_BOT')
    reuters_bot = create_bot(
        config=config,
        articles_source_repository=ReutersRepository(ReutersDataSource(logger)),
        articles_collection_period=600,
        routing_key='article-queue03',
        logger=logger,
    )
    reuters_bot.run()
else:
    print('Could not create Reuters bot')
