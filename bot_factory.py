import getopt

from data.data_destinations.aggregator_data_destination import AggregatorDataDestination
from data.repositories.aggergator_articles_destination_repository import AggregatorArticlesDestinationRepository
from domain.models.bot import Bot


def create_bot(argv, articles_source_repository, articles_collection_period):
    try:
        opts, _ = getopt.getopt(argv[1:], "s:", ["server ="])
        server = opts[0][1]
    except (getopt.GetoptError, IndexError):
        print("WRONG USAGE. USAGE: --server <server with queue IP>")
        return None

    return Bot(
        articles_source_repository=articles_source_repository,
        articles_destination_repository=AggregatorArticlesDestinationRepository(AggregatorDataDestination(server)),
        articles_collection_period_sec=articles_collection_period,
    )
