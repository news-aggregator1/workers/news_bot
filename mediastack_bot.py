from data.data_sources.mediastack_data_source import MediastackDataSource
from data.repositories.mediastack_repository import MediastackRepository
from domain.bot.bot_factory import create_bot
from config_reader import read_config
from domain.logger import Logger


DAILY_REQUESTS_LIMIT = 15

config = read_config()
if config is not None:
    news_api_bot = create_bot(
        config=config,
        articles_source_repository=MediastackRepository(MediastackDataSource(api_key=config.articles_source_api_key)),
        articles_collection_period=0.9 * ((24 * 3600) / DAILY_REQUESTS_LIMIT),
        routing_key='article-queue04',
        logger=Logger('MEDIASTACK_BOT'),
    )
    news_api_bot.run()
else:
    print('Could not create Mediastack bot')
